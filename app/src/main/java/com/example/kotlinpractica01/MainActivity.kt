package com.example.kotlinpractica01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnSaludar: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnCerrar: Button
    private lateinit var lblSaludar: TextView
    private lateinit var txtNombre: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Relacion de los objetos Kotlin con las vistas xml
        btnSaludar = findViewById(R.id.btnSaludar)
        lblSaludar = findViewById(R.id.lblSaludar)
        txtNombre = findViewById(R.id.txtNombre)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCerrar = findViewById(R.id.btnCerrar)

        //Codificar evento click boton
        btnSaludar.setOnClickListener {
            if (txtNombre.text.toString().matches(Regex("^\\s*$"))) {
                //Falta capturar el nombre
                Toast.makeText(
                    this@MainActivity,
                    "por favor ingresar su nombre", Toast.LENGTH_SHORT
                ).show()
            } else {
                val saludar = txtNombre.text.toString()
                lblSaludar.text = "Hola $saludar, ¿Cómo estás?"
            }
        }

        btnLimpiar.setOnClickListener {
            // Agregue el código para limpiar el EditText aquí
            txtNombre.setText("")
            lblSaludar.setText("")
            txtNombre.requestFocus()
        }

        btnCerrar.setOnClickListener {
            finish()
        }
    }
}


